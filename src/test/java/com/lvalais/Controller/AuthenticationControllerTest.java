package com.lvalais.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lvalais.MainAppApplication;
import com.lvalais.model.LoginUser;
import com.lvalais.model.User;
import com.lvalais.utils.auth.ApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainAppApplication.class)
@WebAppConfiguration
@ContextConfiguration
@Transactional
public class AuthenticationControllerTest {

    @Autowired
    private AuthenticationManager authenticationManager;


    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;
    ObjectMapper objectMapper = new ObjectMapper();


    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }


    public MvcResult addUserToDB(String userID, User.USERROLE role, Boolean hasAdminRights) throws Exception {
        LoginUser loginUser = new LoginUser();
        if (hasAdminRights) {
            loginUser.setUsername("lampros");
            loginUser.setPassword("password1");
            final Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginUser.getUsername(),
                            loginUser.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        String uri = "/auth/" + role.toString().toLowerCase() + "SignUp";
        User user = new User();
        user.setUserID(userID);
        user.setUserPassword("password1");
        user.setUserEmail("email@example.com");
        user.setUserFullName("fullName");
        user.setUserMobileNumber(123456789);
        user.setUserOrderList(null);
        User user2 = new User(userID, "email@example.com", "fullName", 123456789, "password1", null, User.USERROLE.USER);
        String inputJson = objectMapper.writeValueAsString(user);
        return mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
    }

    @Test
    public void registerUserPassTest() throws Exception {
        MvcResult mvcResult = addUserToDB("lam1", User.USERROLE.USER, false);
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        String content = mvcResult.getResponse().getContentAsString();
        ObjectNode message = objectMapper.createObjectNode();
        message.put("message", User.USERROLE.USER + " is registered successfully");
        message.put("redirect", "/admin/login.html");
        assertEquals(objectMapper.writeValueAsString(message), content);
    }

    @Test
    public void doubleRegisterUserFailTest() throws Exception {
        MvcResult mvcResult0 = addUserToDB("lam1", User.USERROLE.USER, false);
        MvcResult mvcResult = addUserToDB("lam1", User.USERROLE.USER, false);
        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
        String content = mvcResult.getResponse().getContentAsString();
        ObjectNode message = objectMapper.createObjectNode();
        message.put("message", User.USERROLE.USER + " is not registered!");
        message.put("redirect", "/admin/register.html");
        assertEquals(objectMapper.writeValueAsString(message), content);
    }

    @Test
    public void registerAdminPassTest() throws Exception {
        MvcResult mvcResult = addUserToDB("lam1", User.USERROLE.ADMIN, true);
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        String content = mvcResult.getResponse().getContentAsString();
        ObjectNode message = objectMapper.createObjectNode();
        message.put("message", User.USERROLE.ADMIN + " is registered successfully");
        message.put("redirect", "/admin/adminLogin.html");
        assertEquals(objectMapper.writeValueAsString(message), content);
    }

    @Test
    public void registerAdminFailTest() throws Exception {
        try {
            MvcResult mvcResult = addUserToDB("lam3", User.USERROLE.ADMIN, false);
        } catch (Exception e) {
            assertTrue(true);
            return;
        }
        assertTrue(false);
    }

    @Test
    public void doubleRegisterAdminFailTest() throws Exception {
        MvcResult mvcResult0 = addUserToDB("lam1", User.USERROLE.ADMIN, true);
        MvcResult mvcResult = addUserToDB("lam1", User.USERROLE.ADMIN, true);
        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
        String content = mvcResult.getResponse().getContentAsString();
        ObjectNode message = objectMapper.createObjectNode();
        message.put("message", User.USERROLE.ADMIN + " is not registered!");
        message.put("redirect", "/admin/register.html");
        assertEquals(objectMapper.writeValueAsString(message), content);
    }

    @Test
    public void registerEmployeePassTest() throws Exception {
        MvcResult mvcResult = addUserToDB("lam1", User.USERROLE.EMPLOYEE, true);
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        String content = mvcResult.getResponse().getContentAsString();
        ObjectNode message = objectMapper.createObjectNode();
        message.put("message", User.USERROLE.EMPLOYEE + " is registered successfully");
        message.put("redirect", "/admin/login.html");
        assertEquals(objectMapper.writeValueAsString(message), content);
    }

    @Test
    public void doubleRegisterEmployeeFailTest() throws Exception {
        MvcResult mvcResult0 = addUserToDB("lam1", User.USERROLE.EMPLOYEE, true);
        MvcResult mvcResult = addUserToDB("lam1", User.USERROLE.EMPLOYEE, true);
        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
        String content = mvcResult.getResponse().getContentAsString();
        ObjectNode message = objectMapper.createObjectNode();
        message.put("message", User.USERROLE.EMPLOYEE + " is not registered!");
        message.put("redirect", "/admin/register.html");
        assertEquals(objectMapper.writeValueAsString(message), content);
    }


    @Test
    public void authenticateRegisteredUserTest() throws Exception {
        MvcResult mvcResult = addUserToDB("lam1", User.USERROLE.USER, false);
        int status = mvcResult.getResponse().getStatus();
        // 201 since new resource is created
        assertEquals(201, status);
        String uri = "/auth/login";
        LoginUser loginUser = new LoginUser();
        loginUser.setUsername("lam1");
        loginUser.setPassword("password1");
        String inputJson = objectMapper.writeValueAsString(loginUser);
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status2 = mvcResult2.getResponse().getStatus();
        assertEquals(202, status2);
    }

    @Test
    public void authenticateNonRegisteredUserTest() throws Exception {
        String uri = "/auth/login";
        LoginUser loginUser = new LoginUser();
        loginUser.setUsername("lam1");
        loginUser.setPassword("password1");
        String inputJson = objectMapper.writeValueAsString(loginUser);
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status2 = mvcResult2.getResponse().getStatus();
        assertEquals(401, status2);
    }


    @Test
    public void lockedLoginTest() throws Exception {
        String uri = "/auth/login";
        LoginUser loginUser = new LoginUser();
        loginUser.setUsername("lam2");
        loginUser.setPassword("password1");
        String inputJson = objectMapper.writeValueAsString(loginUser);
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status2 = mvcResult2.getResponse().getStatus();
        assertEquals(423, status2);
    }

}
