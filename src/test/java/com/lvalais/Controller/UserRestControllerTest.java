package com.lvalais.Controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lvalais.MainAppApplication;
import com.lvalais.model.LoginUser;
import com.lvalais.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainAppApplication.class)
@WebAppConfiguration
@Transactional
public class UserRestControllerTest {

    @Autowired
    private AuthenticationManager authenticationManager;


    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;
    ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        LoginUser loginUser = new LoginUser();
        loginUser.setUsername("lampros");
        loginUser.setPassword("password1");
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public MvcResult addUserToDB(String userID) throws Exception {
        String uri = "/auth/employeeSignUp";
        User user = new User();
        user.setUserID(userID);
        user.setUserPassword("123456");
        user.setUserEmail("email@example.com");
        user.setUserFullName("fullName");
        user.setUserMobileNumber(new Long(123456789));
        user.setUserOrderList(null);
        user.setUserRole(User.USERROLE.EMPLOYEE);

        String inputJson = objectMapper.writeValueAsString(user);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        return mvcResult;
    }

    //Testing GET
    @Test
    public void getUsersList() throws Exception {
        List<User> userList = new ArrayList<>();
        addUserToDB("user2");
        addUserToDB("user3");
        String uri = "/users/getAllUsers";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        userList = objectMapper.readValue(content, new TypeReference<List<User>>() {
        });
        assertTrue(userList.size() > 0);
    }


    @Test
    public void getSingleUserAndEditTest() throws Exception {
        String uri = "/users/getUser";
        addUserToDB("user12345");

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .param("userID", "user12345")).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        User user = objectMapper.readValue(content, User.class);
        assertTrue(user.getUserID().equals("user12345"));
        assertTrue(user.getUserRole().equals(User.USERROLE.EMPLOYEE));

        //Edit user
        user.setUserMobileNumber(122222222);
        user.setUserRole(User.USERROLE.ADMIN);
        String inputJson = objectMapper.writeValueAsString(user);
        String uri2 = "/users/editUser";
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri2)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        //get user to validate
        uri = "/users/getUser";
        mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .param("userID", "user12345")).andReturn();

        status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        content = mvcResult.getResponse().getContentAsString();
        User user2 = objectMapper.readValue(content, User.class);
        assertTrue(user2.getUserID().equals("user12345"));
        assertTrue(user2.getUserRole().equals(User.USERROLE.ADMIN));
        assertTrue(user2.getUserMobileNumber() == 122222222);

        //delete user
        uri2 = "/users/deleteUser";
        mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri2)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param("userID", user2.getUserID())).andReturn();
        String content2 = mvcResult2.getResponse().getContentAsString();

        // validate deletion
        uri = "/users/getAllUsers";
        mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        content = mvcResult.getResponse().getContentAsString();
        List<User> userList = objectMapper.readValue(content, new TypeReference<List<User>>() {
        });
        // 1 user always exists in db, is the admin
        assertTrue(userList.size() == 1);

    }
}
