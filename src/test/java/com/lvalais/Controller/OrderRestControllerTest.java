package com.lvalais.Controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lvalais.MainAppApplication;
import com.lvalais.model.LoginUser;
import com.lvalais.model.Order;
import com.lvalais.model.Product;
import com.lvalais.utils.auth.TokenProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainAppApplication.class)
@WebAppConfiguration
@Transactional
public class OrderRestControllerTest {
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;
    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenProvider jwtTokenUtil;

    Cookie cookie;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        LoginUser loginUser = new LoginUser();
        loginUser.setUsername("lampros");
        loginUser.setPassword("password1");
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        String jwt = jwtTokenUtil.generateToken(authentication);
        cookie = new Cookie("access_token",jwt);

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public MvcResult addOrder(UUID orderUUID, String userID) throws Exception {
        String uri = "/orders/addOrder";
        Order order = new Order();
        order.setOrderUUID(orderUUID);
        order.setOrderPrice(new Double(50.60));
        order.setOrderPostageAddress("Order postage address");
        order.setOrderDate(new Date());
        order.setUserID(userID);
        Product p = new Product();
        p.setProductPrice(10);
        p.setProductUUID(UUID.fromString("6701f6ca-6f89-49c8-bb3b-57bd50045156"));
        p.setProductName("product name");
        p.setProductType(Product.ProductType.CAKE);
        List<String> ing = new ArrayList<>();
        ing.add("Sugar");
        p.setProductIngredients(ing);
        List<Product> productList = new ArrayList<>();
        productList.add(p);
        order.setOrderProductList(productList);
        String inputJson = objectMapper.writeValueAsString(order);
//        String inputJson = "{\"orderUUID\":null,\"orderPostageAddress\":\"Parsons Green Terrace\",\"orderPrice\":50.5,\"orderDate\":null,\"orderProductList\":[{\"productName\":null,\"productType\":null,\"productUUID\":null,\"productPrice\":null,\"productDescription\":null,\"productShortDescription\":null,\"productIngredients\":[\"Sugar\",\"Oil\",\"Flour\"]}],\"userID\":null}";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson).cookie(cookie)).andReturn();
        return mvcResult;
    }

    @Test
    public void addNewOrderTest() throws Exception {
        ObjectNode message = objectMapper.createObjectNode();
        UUID uuid = UUID.fromString("6701f6ca-6f89-49c8-bb3b-57bd50045146");
        MvcResult mvcResult = addOrder(uuid, "user12345");
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        String content = mvcResult.getResponse().getContentAsString();

        message.put("message","Order added successfully!");
        assertEquals( objectMapper.writeValueAsString(message),content);
    }

    @Test
    public void getSingleOrderTest() throws Exception {
        UUID uuid = UUID.fromString("6701f6ca-6f89-49c8-bb3b-57bd50045146");
        MvcResult mvcResult = addOrder(uuid, "user12345");

        String uri2 = "/orders/getOrder";
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.get(uri2)
                .param("orderUUID", uuid.toString())).andReturn();
        int status = mvcResult2.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult2.getResponse().getContentAsString();
        Order newOrder = objectMapper.readValue(content, Order.class);

        assertTrue(newOrder.getOrderUUID().equals(uuid));
        assertTrue(newOrder.getUserID().equals("lampros"));
    }


    @Test
    public void getAllOrdersTest() throws Exception {
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        addOrder(uuid1, "user12345");
        addOrder(uuid2, "user123456");

        String uri2 = "/orders/getAllOrders";
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.get(uri2)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult2.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult2.getResponse().getContentAsString();
        List<Order> orderList = objectMapper.readValue(content, new TypeReference<List<Order>>() {
        });
        assertTrue(orderList.size() == 2);
    }

    @Test
    public void getTodaysOrdersTest() throws Exception {
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        addOrder(uuid1, "user12345");
        addOrder(uuid2, "user123456");

        String uri2 = "/orders/getTodaysOrders";
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.get(uri2)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult2.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult2.getResponse().getContentAsString();
        List<Order> orderList = objectMapper.readValue(content, new TypeReference<List<Order>>() {
        });
        assertTrue(orderList.size() == 2);
    }

    @Test
    public void editOrderTest() throws Exception {
        //add order
        String uri = "/orders/addOrder";
        Order order = new Order();
        order.setOrderUUID(UUID.randomUUID());
        order.setOrderPrice(new Double(50.60));
        order.setOrderPostageAddress("Order postage address");
        order.setOrderDate(new Date());
        order.setUserID("user12345");
        order.setOrderProductList(new ArrayList<>());
        String inputJson = objectMapper.writeValueAsString(order);
        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        // Edit order
        order.setOrderPrice(new Double(66.13));
        order.setOrderPostageAddress("New order postage address");
        inputJson = objectMapper.writeValueAsString(order);
        String uri2 = "/orders/editOrder";
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri2)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult2.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult2.getResponse().getContentAsString();
        assertEquals(content, "Order changed!");

        // Verify order changed
        String uri3 = "/orders/getOrder";
        MvcResult mvcResult3 = mvc.perform(MockMvcRequestBuilders.get(uri3)
                .param("orderUUID", order.getOrderUUID().toString())).andReturn();
        int status2 = mvcResult3.getResponse().getStatus();
        assertEquals(200, status2);
        String content2 = mvcResult3.getResponse().getContentAsString();
        Order editedOrder = objectMapper.readValue(content2, Order.class);

        assertTrue(editedOrder.getOrderUUID().equals(order.getOrderUUID()));
        assertTrue(editedOrder.getUserID().equals("user12345"));
        assertTrue(editedOrder.getOrderPostageAddress().equals("New order postage address"));
        assertTrue(editedOrder.getOrderPrice() == 66.13);
    }


}
