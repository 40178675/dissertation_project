package com.lvalais.Controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lvalais.MainAppApplication;
import com.lvalais.model.LoginUser;
import com.lvalais.model.Product;
import com.lvalais.utils.auth.TokenProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

//import org.junit.Test;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainAppApplication.class)
@WebAppConfiguration
@Transactional
public class ProductRestControllerTest {
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;
    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider jwtTokenUtil;

    Cookie cookie;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        LoginUser loginUser = new LoginUser();
        loginUser.setUsername("lampros");
        loginUser.setPassword("password1");
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        String jwt = jwtTokenUtil.generateToken(authentication);
        cookie = new Cookie("access_token",jwt);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }


    public MvcResult addProduct(String productName, Product.ProductType productType) throws Exception {
        String uri = "/products/addProduct";
        Product product = new Product();
        product.setProductUUID(UUID.randomUUID());
        product.setProductShortDescription("Short description");
        product.setProductDescription("Long description");
        product.setProductPrice(new Double(50));
        product.setProductType(productType);
        product.setProductName(productName);
        List<String> ingredientList = new ArrayList<>();
        ingredientList.add("Sugar");
        ingredientList.add("Flour");
        ingredientList.add("Puff_pastry");
        product.setProductIngredients(ingredientList);

        String inputJson = objectMapper.writeValueAsString(product);
        return mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson).cookie(cookie)).andReturn();

    }

    @Test
    public void addNewProductTest() throws Exception {
        MvcResult mvcResult = addProduct("Muffin", Product.ProductType.MUFFIN);
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, "New product stored!");
    }


    @Test
    public void getAllProductsTest() throws Exception {
        List<Product> productList = new ArrayList<>();
        addProduct("Cake", Product.ProductType.CAKE);
        addProduct("Muffin2", Product.ProductType.MUFFIN);

        String uri = "/products/getAllProducts";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        productList = objectMapper.readValue(content, new TypeReference<List<Product>>() {
        });
        assertTrue(productList.size() == 4);
    }


    @Test
    public void getSingleProductTest() throws Exception {

        String uri = "/products/addProduct";
        Product product = new Product();
        product.setProductUUID(UUID.randomUUID());
        product.setProductShortDescription("Short description");
        product.setProductDescription("Long description");
        product.setProductPrice(new Double(50));
        product.setProductType(Product.ProductType.MUFFIN);
        product.setProductName("Muffin2");
        product.setProductIngredients(new ArrayList<>());

        String inputJson = objectMapper.writeValueAsString(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        String uri2 = "/products/getProduct";
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.get(uri2)
                .param("productUUID", product.getProductUUID().toString())).andReturn();
        int status = mvcResult2.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult2.getResponse().getContentAsString();
        Product newProduct = objectMapper.readValue(content, Product.class);
        assertTrue(newProduct.getProductDescription().equals(product.getProductDescription()));
        assertTrue(newProduct.getProductName().equals(product.getProductName()));
    }


    @Test
    public void editAndRemoveProductTest() throws Exception {
        //add product
        String uri = "/products/addProduct";
        Product product = new Product();
        product.setProductUUID(UUID.randomUUID());
        product.setProductShortDescription("Short description");
        product.setProductDescription("Long description");
        product.setProductPrice(new Double(50));
        product.setProductType(Product.ProductType.MUFFIN);
        product.setProductName("Muffin2");
        product.setProductIngredients(new ArrayList<>());

        String inputJson = objectMapper.writeValueAsString(product);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        //edit product
        List<String> list = new ArrayList<>();
        list.add("sugar");
        product.setProductIngredients(list);
        product.setProductPrice(59.60);
        inputJson = objectMapper.writeValueAsString(product);
        uri = "/products/editProduct";
        mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        // verify item is updated
        uri = "/products/getProduct";
        mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).param("productUUID", product.getProductUUID().toString())).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        Product newProduct = objectMapper.readValue(content, Product.class);
        assertTrue(newProduct.getProductIngredients().size() == 1);

        //remove product
        String uri2 = "/products/removeProduct";
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.delete(uri2)
                .param("productUUID", product.getProductUUID().toString())).andReturn();
        status = mvcResult2.getResponse().getStatus();
        assertEquals(200, status);
        // verify product removed
        String uri3 = "/products/getAllProducts";
        MvcResult mvcResult3 = mvc.perform(MockMvcRequestBuilders.get(uri3)
                .param("productUUID", product.getProductUUID().toString())).andReturn();
        int status2 = mvcResult3.getResponse().getStatus();
        assertEquals(200, status2);
        content = mvcResult3.getResponse().getContentAsString();
        List<Product> productList = new ArrayList<>();
        productList = objectMapper.readValue(content, new TypeReference<List<Product>>() {
        });
        assertTrue(productList.size() == 2);
    }


}
