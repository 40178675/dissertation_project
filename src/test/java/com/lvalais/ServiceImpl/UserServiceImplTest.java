package com.lvalais.ServiceImpl;

import com.lvalais.DaoInterface.UserDao;
import com.lvalais.MainAppApplication;
import com.lvalais.Service.UserService;
import com.lvalais.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainAppApplication.class)
@WebAppConfiguration
@ContextConfiguration
@Transactional
public class UserServiceImplTest {

    UserDao userDao;
    @Autowired
    UserService userService;

    @Test
    public void findOneTest() {
        User user = userService.findOne("lampros");
        assertTrue(user.getUserID().equals("lampros"));
    }

    @Test
    public void findAllTest() {
        List list = userService.findAll();
        assertTrue(list.size() == 1);
    }

    @Test
    public void saveTest() {
        User user = new User();
        user.setUserID("lam");
        user.setUserPassword("password1");
        user.setUserEmail("email@example.com");
        user.setUserFullName("fullName");
        user.setUserMobileNumber(123456789);
        user.setUserOrderList(null);
        user.setUserRole(User.USERROLE.USER);
        userService.save(user);
        List list = userService.findAll();
        assertTrue(list.size() == 2);
    }

    @Test
    public void deleteTest() {
        userService.delete("lampros");
        List list = userService.findAll();
        assertTrue(list.size() == 0);
    }
}
