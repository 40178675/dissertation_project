package com.lvalais.utils.auth;

import com.lvalais.MainAppApplication;
import com.lvalais.model.LoginUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainAppApplication.class)
@WebAppConfiguration
@Transactional
public class JwtAuthenticationFilterTest {

    @Autowired
    JwtAuthenticationFilter jwtAuthenticationFilter;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    TokenProvider tokenProvider = new TokenProvider();

    @Test
    public void doFilterInternalTest() {

        LoginUser loginUser = new LoginUser();
        loginUser.setUsername("lampros");
        loginUser.setPassword("password1");
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String result = tokenProvider.generateToken(authentication);


        MockHttpServletRequest request = new MockHttpServletRequest();
        Cookie cookie = new Cookie("name", "aValue");
        Cookie cookie1 = new Cookie("access_token", result);
        request.setCookies(cookie);
        request.setCookies(cookie1);


        request.addHeader("Content-Type", "text/html");
        request.addHeader("Status", "HTTP/1.1 200 OK");


        request.addHeader("Authorization", "Bearer: " + result);
        MockHttpServletResponse response = new MockHttpServletResponse();

        MockFilterChain filterChain = new MockFilterChain();
        SecurityContextHolder.getContext().setAuthentication(null);
        try {
            jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);
        } catch (Exception e) {
            assertTrue(false);
        }
        assertTrue(true);

    }

}
