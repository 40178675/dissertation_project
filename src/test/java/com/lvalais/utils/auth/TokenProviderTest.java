package com.lvalais.utils.auth;

import com.lvalais.MainAppApplication;
import com.lvalais.model.LoginUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainAppApplication.class)
@WebAppConfiguration
@Transactional
public class TokenProviderTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    TokenProvider tokenProvider = new TokenProvider();

    @Test
    public void generateTokenTest() {
        LoginUser loginUser = new LoginUser();
        loginUser.setUsername("lampros");
        loginUser.setPassword("password1");
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String result = tokenProvider.generateToken(authentication);
        UserDetails userDetails = userDetailsService.loadUserByUsername("lampros");
        assertTrue(tokenProvider.validateToken(result, userDetails));
        UsernamePasswordAuthenticationToken auth = tokenProvider.getAuthentication(result, SecurityContextHolder.getContext().getAuthentication(), userDetails);
        GrantedAuthority first = auth.getAuthorities().iterator().next();
        assertTrue(first.getAuthority().equals("ROLE_ADMIN"));
    }

}
