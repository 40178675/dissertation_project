package com.lvalais.model;

import com.lvalais.MainAppApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainAppApplication.class)
@WebAppConfiguration
@ContextConfiguration
public class RoleTest {

    @Test
    public void roleTest() {
        Role newRole = new Role();
        newRole.setDescription("description");
        newRole.setName("name");
        newRole.setUserID("userID");

        assertEquals("description", newRole.getDescription());
        assertEquals("name", newRole.getName());
        assertEquals("userID", newRole.getUserID());
    }


}
