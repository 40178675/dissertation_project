package com.lvalais.model;

import com.lvalais.MainAppApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainAppApplication.class)
@WebAppConfiguration
@ContextConfiguration
public class ConstantsTest {

    @Test
    public void constantsTest() {
        Constants constants = new Constants();
        String TOKEN_PREFIX = "Bearer ";
        String HEADER_STRING = "Authorization";
        String AUTHORITIES_KEY = "scopes";
        assertEquals(TOKEN_PREFIX, constants.TOKEN_PREFIX);
        assertEquals(HEADER_STRING, constants.HEADER_STRING);
        assertEquals(AUTHORITIES_KEY, constants.AUTHORITIES_KEY);
    }


}
