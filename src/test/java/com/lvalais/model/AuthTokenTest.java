package com.lvalais.model;

import com.lvalais.MainAppApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainAppApplication.class)
@WebAppConfiguration
@ContextConfiguration
public class AuthTokenTest {

    @Test
    public void authTokenTest() {
        AuthToken authToken = new AuthToken();
        authToken.setToken("new token");

        AuthToken authToken1 = new AuthToken("new token1");

        assertEquals("new token", authToken.getToken());
        assertEquals("new token1", authToken1.getToken());
    }


}
