package com.lvalais.utils;

import com.lvalais.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        logger.warn("Inside usermapper");

        User user = new User();
        user.setUserID(resultSet.getString("userID"));
        user.setUserMobileNumber(resultSet.getLong("userMobileNumber"));
        user.setUserRole(User.USERROLE.valueOf(resultSet.getString("userRole")));
        user.setUserFullName(resultSet.getString("userFullName"));
        user.setUserEmail(resultSet.getString("userEmail"));
        user.setUserPassword(resultSet.getString("userPassword"));
        return user;
    }
}
