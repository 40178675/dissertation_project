package com.lvalais.utils;

import com.lvalais.model.Product;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class ProductMapper implements RowMapper<Product> {

    public Product mapRow(ResultSet resultSet, int i) throws SQLException {

        Product product = new Product();
        product.setProductName(resultSet.getString("productName"));
        product.setProductUUID(UUID.fromString(resultSet.getString("productUUID")));
        product.setProductType(Product.ProductType.valueOf(resultSet.getString("productType")));
        product.setProductPrice(resultSet.getDouble("productPrice"));
        product.setProductDescription(resultSet.getString("productDescription"));
        product.setProductShortDescription(resultSet.getString("productShortDescription"));
        return product;
    }
}