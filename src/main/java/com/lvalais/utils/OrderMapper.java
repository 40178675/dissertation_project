package com.lvalais.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lvalais.model.Order;
import com.lvalais.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class OrderMapper implements RowMapper<Order> {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    ProductMapper productMapper = new ProductMapper();
    ObjectMapper objectMapper = new ObjectMapper();

    public Order mapRow(ResultSet resultSet, int i) throws SQLException {

        Order order = new Order();
        order.setOrderDate(resultSet.getDate("orderDate"));
        order.setOrderPostageAddress(resultSet.getString("orderPostageAddress"));
        order.setOrderPrice(resultSet.getDouble("orderPrice"));
        order.setOrderUUID(UUID.fromString(resultSet.getString("orderUUID")));
        return order;
    }
}