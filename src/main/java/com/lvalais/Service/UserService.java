package com.lvalais.Service;

import com.lvalais.model.User;

import java.util.List;

public interface UserService {
    List findAll();

    void delete(String userID);

    User findOne(String username);

    void save(User user);

}
