package com.lvalais.model;


import java.util.List;

public class User {
    public enum USERROLE {ADMIN, EMPLOYEE, USER}

    private String userID;
    private String userEmail;
    private String userFullName;
    private long userMobileNumber;
    private String userPassword;
    private List<Order> userOrderList;
    private USERROLE userRole;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public long getUserMobileNumber() {
        return userMobileNumber;
    }

    public void setUserMobileNumber(long userMobileNumber) {
        this.userMobileNumber = userMobileNumber;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public List<Order> getUserOrderList() {
        return userOrderList;
    }

    public void setUserOrderList(List<Order> userOrderList) {
        this.userOrderList = userOrderList;
    }

    public USERROLE getUserRole() {
        return userRole;
    }

    public void setUserRole(USERROLE userRole) {
        this.userRole = userRole;
    }

    public User() {
    }

    public User(String userID, String userEmail, String userFullName, long userMobileNumber, String userPassword, List<Order> userOrderList, USERROLE userRole) {
        this.userID = userID;
        this.userEmail = userEmail;
        this.userFullName = userFullName;
        this.userMobileNumber = userMobileNumber;
        this.userPassword = userPassword;
        this.userOrderList = userOrderList;
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        return "userID: " + userID + ", userEmail: " + userEmail + ", userFullName: " +
                userFullName + ", userMobileNumber: " + userMobileNumber + ", userPassword: " + userPassword + ", userRole: " + userRole;
    }


}
