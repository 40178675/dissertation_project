package com.lvalais.model;


import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class Product {

    public enum ProductType {CAKE, MUFFIN, COOKIE, OTHER}

    private String productName;
    private ProductType productType;

    private UUID productUUID = null;
    private double productPrice;
    private String productDescription;
    private String productShortDescription;
    private List<String> productIngredients = new ArrayList<>();

    public String getProductShortDescription() {
        return productShortDescription;
    }

    public void setProductShortDescription(String productShortDescription) {
        this.productShortDescription = productShortDescription;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public UUID getProductUUID() {
        return productUUID;
    }

    public void setProductUUID(UUID productUUID) {
        this.productUUID = productUUID;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public List<String> getProductIngredients() {
        return productIngredients;
    }

    public void setProductIngredients(List<String> productIngredients) {
        this.productIngredients = productIngredients;
    }
}
