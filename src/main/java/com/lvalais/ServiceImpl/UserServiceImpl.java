package com.lvalais.ServiceImpl;

import com.lvalais.DaoInterface.UserDao;
import com.lvalais.Service.UserService;
import com.lvalais.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserDao userDao;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findUser(username);
        //TODO add check for blocked users here?
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUserID(), user.getUserPassword(), getAuthority(user));
    }

    private Set getAuthority(User user) {
        Set authorities = new HashSet<>();
        try {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getUserRole().toString()));
        } catch (NullPointerException e) {
            logger.error("No authorities found for userID: {}.", user.getUserID());
        }
        return authorities;
    }

    public List findAll() {
        List list = new ArrayList<>();
        userDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    public void delete(String userID) {
        userDao.deleteUser(userID);
    }

    public User findOne(String username) {
        return userDao.findUser(username);
    }

    public void save(User user) {
        User newUser = new User();
        newUser.setUserID(user.getUserID());
        newUser.setUserPassword(bcryptEncoder.encode(user.getUserPassword()));
        newUser.setUserEmail(user.getUserEmail());
        newUser.setUserFullName(user.getUserFullName());
        newUser.setUserMobileNumber(user.getUserMobileNumber());
        newUser.setUserOrderList(user.getUserOrderList());
        newUser.setUserRole(user.getUserRole());
        userDao.addUser(newUser);

    }

}