package com.lvalais.DaoInterface;

import com.lvalais.model.User;

import java.util.List;

public interface UserDao {

    Boolean addUser(User user);

    void editUser(User user);

    void deleteUser(String userID);

    User findUser(String userID);

    List<User> findAll();

    Boolean addLoginAttempt(String userID);



}
