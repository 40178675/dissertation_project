package com.lvalais.DaoInterface;

import com.lvalais.model.Product;

import java.util.List;
import java.util.UUID;

public interface ProductDao {

    void addProduct(Product product);

    void editProduct(Product product);

    void deleteProduct(UUID productUUID);

    Product findProduct(UUID productUUID);

    List<Product> findAll();
}
