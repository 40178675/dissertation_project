package com.lvalais.DaoInterface;

import com.lvalais.model.Order;

import java.util.List;
import java.util.UUID;

public interface OrderDao {

    Boolean addOrder(Order order);

    void editOrder(Order order);

    void deleteOrder(UUID orderUUID);

    Order findOrder(UUID orderUUID);

    List<Order> findAll();

    List<Order> findTodaysOrder();
}
