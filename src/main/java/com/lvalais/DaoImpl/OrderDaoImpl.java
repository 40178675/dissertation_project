package com.lvalais.DaoImpl;

import com.lvalais.DaoInterface.OrderDao;
import com.lvalais.model.Order;
import com.lvalais.model.Product;
import com.lvalais.utils.OrderMapper;
import com.lvalais.utils.ProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
@Qualifier("orderDao")
public class OrderDaoImpl implements OrderDao {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Boolean addOrder(Order order) {
        try {
            String insq = "INSERT INTO order_table VALUES(?,?,?,?)";
            jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
                @Override
                public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                    ps.setString(1, order.getOrderUUID().toString());
                    ps.setDouble(2, order.getOrderPrice());
                    ps.setObject(3, new java.sql.Date(order.getOrderDate().getTime()));
                    ps.setString(4, order.getOrderPostageAddress());
                    return ps.execute();
                }
            });

            insq = "INSERT INTO userorders_table VALUES(?,?)";
            jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
                @Override
                public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                    ps.setString(1, order.getUserID());
                    ps.setString(2, order.getOrderUUID().toString());
                    return ps.execute();
                }
            });


            for (Product product : order.getOrderProductList()) {
                insq = "INSERT INTO orderproducts_table VALUES(?,?)";
                jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
                    @Override
                    public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                        ps.setString(1, order.getOrderUUID().toString());
                        ps.setString(2, product.getProductUUID().toString());
                        return ps.execute();
                    }
                });
            }
            logger.info("Order {} for user {} stored with success",order.getOrderUUID(), order.getUserID());
            return true;
        }catch (Exception e){
            logger.error("Error found: {}",e);
            return false;
        }
    }

    @Override
    public void editOrder(Order order) {
        deleteOrder(order.getOrderUUID());
        addOrder(order);
    }

    @Override
    public void deleteOrder(UUID orderUUID) {
        String insq = "DELETE FROM order_table WHERE orderUUID = ?";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, orderUUID.toString());
                return ps.execute();
            }
        });

        insq = "DELETE FROM orderproducts_table WHERE orderUUID = ?";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, orderUUID.toString());
                return ps.execute();
            }
        });
        insq = "DELETE FROM userorders_table WHERE orderUUID = ?";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, orderUUID.toString());
                return ps.execute();
            }
        });
    }

    @Override
    public Order findOrder(UUID orderUUID) {
        Order order = new Order();
        String sql = "SELECT * FROM order_table WHERE orderUUID=?";
        order = (Order) jdbcTemplate.queryForObject(sql, new Object[]{orderUUID.toString()}, new OrderMapper());
        sql = "SELECT productUUID FROM orderproducts_table WHERE orderUUID = ?";
        List<Product> productList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Product.class),orderUUID.toString());


        order.setOrderProductList(productList);
        sql = "SELECT userID FROM userorders_table WHERE orderUUID = ?";
        String userID = jdbcTemplate.queryForObject(sql, new Object[]{orderUUID.toString()}, String.class);
        order.setUserID(userID);
        return order;
    }

    @Override
    public List<Order> findAll() {
        List<Order> orderListTemp = new ArrayList<>();
        List<Order> orderList = new ArrayList<>();

        String sql = "SELECT * FROM order_table";
        orderListTemp = jdbcTemplate.query(sql,
                new BeanPropertyRowMapper<>(Order.class));
        for (Order order : orderListTemp) {
            sql = "SELECT productUUID FROM orderproducts_table WHERE orderUUID = ?";
            List<String> productListUUID = jdbcTemplate.query(sql, new RowMapper<String>() {
                public String mapRow(ResultSet rs, int rowNum)
                        throws SQLException {
                    return rs.getString(1);
                }
            }, order.getOrderUUID().toString());

            List<Product> productList = new ArrayList<>();
            for(String s: productListUUID) {
                sql = "SELECT * FROM product_table WHERE productUUID = ?";
                productList.add(jdbcTemplate.queryForObject(sql, new Object[]{s}, new ProductMapper()));
            }
            order.setOrderProductList(productList);
            String insq = "SELECT userID FROM userorders_table WHERE orderUUID=?";
            order.setUserID(jdbcTemplate.queryForObject(insq, new Object[]{order.getOrderUUID().toString()}, String.class));
            orderList.add(order);
        }
        return orderList;
    }

    @Override
    public List<Order> findTodaysOrder() {
        List<Order> orderList = new ArrayList<>();
        List<Order> orderListTemp = new ArrayList<>();
        String sql = "SELECT * FROM order_table WHERE orderDate=?";
        orderListTemp = jdbcTemplate.query(sql,
                new BeanPropertyRowMapper<>(Order.class), new java.sql.Date(new Date().getTime()));
        for (Order order : orderListTemp) {
            sql = "SELECT productUUID FROM orderproducts_table WHERE orderUUID = ?";
            List<String> productListUUID = jdbcTemplate.query(sql, new RowMapper<String>() {
                public String mapRow(ResultSet rs, int rowNum)
                        throws SQLException {
                    return rs.getString(1);
                }
            }, order.getOrderUUID().toString());

            List<Product> productList = new ArrayList<>();
            for(String s: productListUUID) {
                sql = "SELECT * FROM product_table WHERE productUUID = ?";
                productList.add(jdbcTemplate.queryForObject(sql, new Object[]{s}, new ProductMapper()));
            }
            order.setOrderProductList(productList);
            String insq = "SELECT userID FROM userorders_table WHERE orderUUID=?";
            order.setUserID(jdbcTemplate.queryForObject(insq, new Object[]{order.getOrderUUID().toString()}, String.class));
            orderList.add(order);
        }
        return orderList;
    }

}