package com.lvalais.DaoImpl;

import com.lvalais.DaoInterface.ProductDao;
import com.lvalais.model.Product;
import com.lvalais.utils.ProductMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
@Qualifier("productDao")
public class ProductDaoImpl implements ProductDao {
    private static final Logger logger = LogManager.getLogger(ProductDaoImpl.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void addProduct(Product product) {
        logger.info("Adding {} to db...", product);
        try{
            product.getProductUUID().equals(null);
        }catch (NullPointerException n){
            logger.warn("No UUID found, creating a new one..");
            product.setProductUUID(UUID.randomUUID());
        }
        String insq = "INSERT INTO product_table VALUES(?,?,?,?,?,?)";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, product.getProductName());
                ps.setString(2, product.getProductType().toString());
                ps.setString(3, product.getProductUUID().toString());
                ps.setDouble(4, product.getProductPrice());
                ps.setString(5, product.getProductDescription());
                ps.setString(6, product.getProductShortDescription());
                return ps.execute();
            }
        });
        for (String ingredient : product.getProductIngredients()) {
            insq = "INSERT INTO ingredients_table VALUES(?,?)";
            jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
                @Override
                public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                    ps.setString(1, product.getProductUUID().toString());
                    ps.setString(2, ingredient);
                    return ps.execute();
                }
            });

        }
        logger.info("Product stored!");
    }

    @Override
    public void editProduct(Product product) {
        deleteProduct(product.getProductUUID());
        addProduct(product);
    }

    @Override
    public void deleteProduct(UUID productUUID) {
        String insq = "DELETE FROM product_table WHERE productUUID = ?";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, productUUID.toString());
                return ps.execute();
            }
        });
        insq = "DELETE FROM ingredients_table WHERE productUUID = ?";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, productUUID.toString());
                return ps.execute();
            }
        });
    }

    @Override
    public Product findProduct(UUID productUUID) {
        Product product = new Product();
        String sql = "SELECT * FROM product_table WHERE productUUID=?";
        try {
            product = jdbcTemplate.queryForObject(sql, new Object[]{productUUID.toString()}, new ProductMapper());
            sql = "SELECT ingredientName FROM ingredients_table WHERE productUUID = ?";
            List<String> ingredientList = jdbcTemplate.query(sql, new RowMapper<String>() {
                public String mapRow(ResultSet rs, int rowNum)
                        throws SQLException {
                    return rs.getString(1);
                }
            }, productUUID.toString());
            product.setProductIngredients(ingredientList);
        } catch (EmptyResultDataAccessException e) {
            logger.error("Error while finding product :" + e);
        }
        return product;
    }

    @Override
    public List<Product> findAll() {
        logger.info("Find all method called...");
        List<Product> productListTemp = new ArrayList<>();
        List<Product> productList = new ArrayList<>();
        String sql = "SELECT * FROM product_table";
        productListTemp = jdbcTemplate.query(sql, new BeanPropertyRowMapper(Product.class));
        logger.info("Temp list size: {}", productListTemp.size());
        for (Product product : productListTemp) {
            sql = "SELECT ingredientName FROM ingredients_table WHERE productUUID = ?";
            List<String> ingredientList = jdbcTemplate.query(sql, new RowMapper<String>() {
                public String mapRow(ResultSet rs, int rowNum)
                        throws SQLException {
                    return rs.getString(1);
                }
            }, product.getProductUUID().toString());
            logger.info("ingredient list size: {}", ingredientList.size());
            product.setProductIngredients(ingredientList);
            productList.add(product);
        }
        logger.info("Final list size: {}", productList.size());
        return productList;
    }


}
