package com.lvalais.DaoImpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lvalais.DaoInterface.UserDao;
import com.lvalais.model.User;
import com.lvalais.utils.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
@Qualifier("userDao")
public class UserDaoImpl implements UserDao {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    ObjectMapper objectMapper = new ObjectMapper();

    @Value("${loginTimeout}")
    private int LOGIN_TIMEOUT;

    @Value("${loginAttempts}")
    private int LOGIN_ATTEMPTS;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Boolean addUser(User user) {
        logger.info("looking to add user to db....");
        try {
            jdbcTemplate.update("INSERT INTO user_table (userId, userFullName, userEmail, userMobileNumber, userPassword, userRole)"
                    + " VALUES (?,?,?,?,?,?)", user.getUserID(), user.getUserFullName(), user.getUserEmail(), user.getUserMobileNumber(), user.getUserPassword(), user.getUserRole().toString());
            logger.info("{} added in db", user.getUserID());
            return true;
        } catch (DuplicateKeyException d) {
            logger.error("{} already exists in db", user.getUserID());
            return false;
        }
    }

    @Override
    public void editUser(User user) {
        deleteUser(user.getUserID());
        addUser(user);
    }

    @Override
    public void deleteUser(String userID) {
        String insq = "DELETE FROM user_table WHERE userID = ?";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, userID.toString());
                return ps.execute();
            }
        });
    }

    @Override
    public User findUser(String userID) {
        logger.info("Retrieving single user...");
        User user = new User();
        String sql = "SELECT * FROM user_table WHERE userID=?";
        try {
            user = jdbcTemplate.queryForObject(sql, new Object[]{userID}, new UserMapper());
        } catch (EmptyResultDataAccessException e) {
            logger.warn("{} not found in database.. : {}", userID, e);
            return null;
        }
        logger.info("Retrieved user : " + user.toString());
        return user;
    }


    @Override
    public List<User> findAll() {
        logger.info("Retrieving all users...");
        List<User> userList = new ArrayList<>();
        String sql = "SELECT * FROM user_table";
//        userList = jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(User.class,false));
        userList = jdbcTemplate.query(sql, new UserMapper());
        for (User user : userList) {
            logger.info("Retrieved user : " + user.toString());
        }
        return userList;
    }

    @Override
    public Boolean addLoginAttempt(String userID){
        logger.info("Checking if user is locked....");
        if (!checkUserLock(userID)) {
            logger.info("looking to add login attempt to db....");
            try {
                jdbcTemplate.update("INSERT INTO persistent_logins (userId, lastUsed)"
                        + " VALUES (?,?)", userID, new Timestamp((new Date()).getTime()));
                logger.info("{} added in db", userID);
                updateLockStatus(userID);
                return true;
            } catch (Exception e) {
                logger.error("Error: " + e);
                return false;
            }



        }else {
            logger.info("User account is locked!");
            return false;
        }
    }

    private void updateLockStatus(String userID){
        String sql = "SELECT userID FROM persistent_logins WHERE userID=?";
        List<String> userIDList = jdbcTemplate.query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int rowNum)
                    throws SQLException {
                return rs.getString(1);
            }
        }, userID);

        if (userIDList.size()==LOGIN_ATTEMPTS){
             sql = "UPDATE locked_accounts SET isLocked = ?, lastUpdated= ? WHERE userID=?";
            jdbcTemplate.update(sql,true,new Timestamp(new Date().getTime()),userID);
            logger.warn("User {} is now locked at {}! ",userID,new Timestamp(new Date().getTime()));
        }
    }

    private Boolean checkUserLock(String userID){
        logger.info("Retrieving user lock status...");
        Boolean status;
        String sql = "SELECT islocked FROM locked_accounts WHERE userID=?";
        try {
             status= jdbcTemplate.queryForObject(sql, new Object[]{userID}, Boolean.class);
             if (status){
                 logger.info("Status is true... checking if prohibition time has passed!");
                 // check if time is expired
                 sql = "SELECT lastUpdated FROM locked_accounts WHERE userID=?";
                 Timestamp t = jdbcTemplate.queryForObject(sql, new Object[]{userID}, Timestamp.class);
                 //TODO compare t with now and reset it if needed

                 logger.warn("Retrieved timestamp, current time is {} and db timestamp was {}",new Timestamp(new Date().getTime()),t);
                 Calendar calendar = Calendar.getInstance();
                 calendar.setTime(t);
                 calendar.add(Calendar.MINUTE,LOGIN_TIMEOUT);
                 Date targetTime = calendar.getTime();
                 if (targetTime.before(new Date())){
                    logger.info("Login Prohibition time expired! Was {}",targetTime);
                     sql = "UPDATE locked_accounts SET isLocked = ?, lastUpdated= ? WHERE userID=?";
                     jdbcTemplate.update(sql,false,new Timestamp(new Date().getTime()),userID);
                     //TODO delete * from login attempts for this user??
                     sql = "DELETE FROM persistent_logins WHERE userID = ?";
                     jdbcTemplate.execute(sql, new PreparedStatementCallback<Boolean>() {
                         @Override
                         public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                             ps.setString(1, userID);
                             return ps.execute();
                         }
                     });
                     logger.info("Login attempts for user {} are now deleted as login prohibition expired.",userID);
                    status = false;
                 }
             }
        } catch (Exception e) {
            logger.warn("{} not found in database.. : {}", userID, e);
            try{
                jdbcTemplate.update("INSERT INTO locked_accounts (userId, isLocked, lastUpdated)"
                        + " VALUES (?,?,?)", userID, false,new Timestamp((new Date()).getTime()));
            }catch (Exception n){
                logger.warn("Error when trying to insert user in locked accounts table for the first time: {}" ,n);
            }
            return false;
        }

        logger.info("Retrieved status for user {} is {} ", userID,status);
        return status;
    }



}
