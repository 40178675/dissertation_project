package com.lvalais.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lvalais.DaoInterface.OrderDao;
import com.lvalais.model.Order;
import com.lvalais.model.Product;
import com.lvalais.utils.auth.TokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.lvalais.model.Constants.HEADER_STRING;
import static com.lvalais.model.Constants.TOKEN_PREFIX;

@RestController
@RequestMapping("/orders")
public class OrderRestController {
    @Autowired
    private OrderDao orderDao;

    @Autowired
    private TokenProvider jwtTokenUtil;
    @Autowired
    private ProductRestController productRestController;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private ObjectMapper objectMapper = new ObjectMapper();


    @PreAuthorize("hasAnyRole('USER','EMPLOYEE','ADMIN')")
    @PostMapping(path = "/addOrder")
    public @ResponseBody
    ResponseEntity<?> addNewOrder(@RequestBody Order order, HttpServletRequest request, HttpServletResponse response) {
        ObjectNode message = objectMapper.createObjectNode();
        logger.info("Adding new order...");
        order.setUserID(getUsernameFromCookie(request));
        order.setOrderDate(new Date());
        try{
           if (order.getOrderUUID().toString().equals(null)){
               order.setOrderUUID(UUID.randomUUID());
            }
        }catch (NullPointerException e){
            order.setOrderUUID(UUID.randomUUID());
        }
        List<Product> productList = new ArrayList<>();
        double cost=0;
        for (Product p:order.getOrderProductList()) {
            Product retrievedProduct = (productRestController.getSingleProduct(p.getProductUUID().toString()));
            productList.add(retrievedProduct);
            cost+= retrievedProduct.getProductPrice();
        }
        order.setOrderPrice(cost);
        order.setOrderProductList(productList);
        // Add welcome message
        if (orderDao.addOrder(order)) {
            message.put("message", "Order added successfully!");
            return ResponseEntity.status(HttpStatus.CREATED).body(message);
        }else{
            message.put("message", "Failed to add the order!");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(message);
        }
    }


    private String getUsernameFromCookie(HttpServletRequest req){
        String header = req.getHeader(HEADER_STRING);
        String username = null;
        try {
            logger.warn("Request cookies : {}", req.getCookies().length);
            if (req.getCookies().length > 0) {
                for (Cookie c : req.getCookies()) {
                    if (c.getName().equals("access_token")) {
                        logger.info("Cookie access token found! Using that for authorization....");
                        try {
                            return jwtTokenUtil.getUsernameFromToken(c.getValue());
                        }catch (Exception e){
                            logger.error("Exception during getting username from cookie");
                        }
                    }
                }
            }

        } catch (NullPointerException n) {
            logger.warn("No cookies found, will search the headers now...");

        }
        return null;
    }

    @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
    @GetMapping(path = "/getAllOrders")
    public @ResponseBody
    Iterable<Order> getAllOrders() {
        logger.info("Getting all orders from db...");
        return orderDao.findAll();
    }

    //Getting single product
    @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
    @GetMapping(path = "/getOrder")
    public @ResponseBody
    Order getSingleOrder(@RequestParam String orderUUID) {
        logger.info("Getting info for order : {}", orderUUID);
        return orderDao.findOrder(UUID.fromString(orderUUID));
    }

    @PreAuthorize("hasAnyRole('EMPLOYEE','ADMIN')")
    @GetMapping(path = "/getTodaysOrders")
    public @ResponseBody
    Iterable<Order> getTodaysOrders() {
        logger.info("Getting todays orders from db...");
        return orderDao.findTodaysOrder();
    }


    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/editOrder")
    public @ResponseBody
    String editOrder(@RequestBody Order order) {
        logger.info("Editing order with uuid {} ... ", order.getOrderUUID());
        orderDao.editOrder(order);
        return "Order changed!";
    }


}
