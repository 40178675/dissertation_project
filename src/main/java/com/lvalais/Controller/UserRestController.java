package com.lvalais.Controller;

import com.lvalais.DaoInterface.UserDao;
import com.lvalais.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserRestController {
    @Autowired
    private UserDao userDao;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    //Getting single user
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(path = "/getUser")
    public @ResponseBody
    User getSingleUser(@RequestParam String userID) {
        logger.info("Getting info for user : {}", userID);
        return userDao.findUser(userID);
    }

    //Getting all users
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(path = "/getAllUsers")
    public @ResponseBody
    List<User> getAllUsers() {
        logger.info("Getting all users from db...");
        return userDao.findAll();
    }

    //delete user
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/deleteUser")
    public @ResponseBody
    String deleteUser(@RequestParam String userID) {
        logger.info("deleting  user {} from db...", userID);
        userDao.deleteUser(userID);
        return "User deleted";
    }

    //edit user
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/editUser")
    public @ResponseBody
    void editUser(@RequestBody User user) {
        logger.info("editing  user {} in db...", user.getUserID());
        userDao.editUser(user);
    }


}
