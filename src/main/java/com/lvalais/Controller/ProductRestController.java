package com.lvalais.Controller;

import com.lvalais.DaoInterface.ProductDao;
import com.lvalais.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.util.UUID;

@RestController
@RequestMapping("/products")
public class ProductRestController {
    @Autowired
    private ProductDao productDao;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @PermitAll
    @PostMapping(path = "/addProduct")
    public @ResponseBody
    String addNewProduct(@RequestBody Product product) {
        logger.info("Adding new product...");
        productDao.addProduct(product);
        return "New product stored!";
    }

    @GetMapping(path = "/getAllProducts")
    public @ResponseBody
    Iterable<Product> getAllProducts() {
        logger.info("Getting all products from db...");
        return productDao.findAll();
    }

    //Getting single product
    @GetMapping(path = "/getProduct")
    public @ResponseBody
    Product getSingleProduct(@RequestParam String productUUID) {
        logger.info("Getting info for product : {}", productUUID);
        return productDao.findProduct(UUID.fromString(productUUID));
    }

    //removing single product
    @DeleteMapping(path = "/removeProduct")
    public @ResponseBody
    String removeProduct(@RequestParam String productUUID) {
        logger.info("Removing record for product : {}", productUUID);
        productDao.deleteProduct(UUID.fromString(productUUID));
        return "Removed";
    }

    //edit product
    @PostMapping(path = "/editProduct")
    public @ResponseBody
    String removeProduct(@RequestBody Product product) {
        logger.info("Removing record for product : {}", product.getProductUUID());
        productDao.editProduct(product);
        return "Removed";
    }

}
