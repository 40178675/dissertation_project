package com.lvalais.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lvalais.DaoInterface.UserDao;
import com.lvalais.model.LoginUser;
import com.lvalais.model.User;
import com.lvalais.utils.auth.JwtAuthenticationResponse;
import com.lvalais.utils.auth.TokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthenticationController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${cookie_age}")
    private int COOKIE_AGE;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider jwtTokenUtil;

    @Autowired
    private UserDao userDao;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    private ObjectMapper objectMapper = new ObjectMapper();

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginUser loginUser, HttpServletRequest request, HttpServletResponse response) {
        ObjectNode message = objectMapper.createObjectNode();
        // todo make check for user lock here
      if (!userDao.addLoginAttempt(loginUser.getUsername())){
          message.put("message","User "+ loginUser.getUsername() + " is locked");
         return ResponseEntity.status(HttpStatus.LOCKED).body(message);
      }else {
          Authentication authentication;
          try {
              authentication = authenticationManager.authenticate(
                      new UsernamePasswordAuthenticationToken(
                              loginUser.getUsername(),
                              loginUser.getPassword()
                      )
              );
              SecurityContextHolder.getContext().setAuthentication(authentication);
          } catch (BadCredentialsException b) {
              logger.error("Bad user credentials or user doesn't exist!");
              return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Bad user credentials or user doesn't exist!");
          } catch (AuthenticationCredentialsNotFoundException a) {
              logger.error("Authentication doesn't exist!");
              return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Bad user credentials or user doesn't exist!");
          }
          String jwt = jwtTokenUtil.generateToken(authentication);
          JwtAuthenticationResponse jwtAuthenticationResponse = new JwtAuthenticationResponse(jwt);
          try {
              logger.info("jwtAuth is: {}", objectMapper.writeValueAsString(jwtAuthenticationResponse));
              // Add jwt as a cookie
              Cookie jwtCookie = new Cookie("access_token", jwt);
              jwtCookie.setPath(request.getContextPath().length() > 0 ? request.getContextPath() : "/");
              // A negative value means that the cookie is not stored persistently and will be deleted when the Web browser exits. A zero value causes the cookie to be deleted.
              jwtCookie.setMaxAge(COOKIE_AGE);
              jwtCookie.setSecure(true);
              //Cookie cannot be accessed via JavaScript
              jwtCookie.setHttpOnly(true);
              //Set JWT as a cookie
              logger.info("Cookie with {} minutes age is created!", (COOKIE_AGE / 60));
              response.addCookie(jwtCookie);
          } catch (JsonProcessingException j) {
              logger.error("Parsing error : {}", j);
          }
          // Add welcome message
          message.put("message", "Welcome back " + loginUser.getUsername());
          //  add redirection details
          if (authentication.getAuthorities().iterator().next().toString().equals("ROLE_ADMIN")) {
              message.put("redirect", "/admin/index.html");
          } else {
              message.put("redirect", "/index.html");
          }
          return ResponseEntity.status(HttpStatus.ACCEPTED).body(message);
      }
    }

    @PostMapping("/userSignUp")
    public ResponseEntity<?> registerUser(@RequestBody User signUpUser, HttpServletRequest request, HttpServletResponse response) {
        signUpUser.setUserRole(User.USERROLE.USER);
        return registerAnyUser(signUpUser);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/adminSignUp")
    public ResponseEntity<?> registerAdmin(@RequestBody User signUpUser, HttpServletRequest request, HttpServletResponse response) {

        signUpUser.setUserRole(User.USERROLE.ADMIN);
        return registerAnyUser(signUpUser);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/employeeSignUp")
    public ResponseEntity<?> registerEmployee(@RequestBody User signUpUser) {
        signUpUser.setUserRole(User.USERROLE.EMPLOYEE);
        return registerAnyUser(signUpUser);

    }

    private ResponseEntity registerAnyUser(User userToRegister) {
        userToRegister.setUserPassword(bcryptEncoder.encode(userToRegister.getUserPassword()));
        if (!userDao.addUser(userToRegister)) {
            logger.error("Setting response to Not acceptable");
            ObjectNode message = objectMapper.createObjectNode();
            message.put("message", userToRegister.getUserRole() + " is not registered!");
            message.put("redirect", "/admin/register.html");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(message);
        } else {
            logger.error("Setting response to accepted");
            ObjectNode message = objectMapper.createObjectNode();
            message.put("message", userToRegister.getUserRole() + " is registered successfully");
            if (userToRegister.getUserRole() == User.USERROLE.ADMIN) {
                message.put("redirect", "/admin/adminLogin.html");
            } else {
                message.put("redirect", "/admin/login.html");
            }
            return ResponseEntity.status(HttpStatus.CREATED).body(message);
        }
    }
}