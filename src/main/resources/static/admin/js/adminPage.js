//admin page
angular.module('adminPage', ['ui.grid']).controller('adminController', ['$location', function ($http, $scope, $location, AuthenticationService) {
    console.info("inside admin index module!");

    var vm = this;
    vm.newMessages = 2;
    vm.newTickets = 2;
    vm.newTasks = 6;
    vm.logout=logout;
    // vm.newOrders = 6;
    fetchTodaysOrders();
    var allOrdersArray = [2000, 2800, 1000, 3660, 5888, 3000, 2227, 3100, 3900, 2900, 4000];
    var labelsArray = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
    var maxValue = 7000;

    // Area Chart Example
    var ctx = document.getElementById("myAreaChart");
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labelsArray,
            datasets: [{
                label: "Sessions",
                lineTension: 0.3,
                backgroundColor: "rgba(2,117,216,0.2)",
                borderColor: "rgba(2,117,216,1)",
                pointRadius: 5,
                pointBackgroundColor: "rgba(2,117,216,1)",
                pointBorderColor: "rgba(255,255,255,0.8)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(2,117,216,1)",
                pointHitRadius: 50,
                pointBorderWidth: 2,
                data: allOrdersArray
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: maxValue,
                        maxTicksLimit: 5
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, .125)",
                    }
                }],
            },
            legend: {
                display: false
            }
        }
    });


    function fetchTodaysOrders() {
        console.info("fetching todays orders");
        $.ajax({
            contentType: "application/json",
            url: "/orders/getTodaysOrders",
            type: "GET",
            xhrFields: {withCredentials: true},
            dataType: "json",
            timeout: 5000,
            success: function (response) {
                console.info("data received back is: " + JSON.stringify(response));
                vm.newOrders = response.length;
                vm.myData = response;

                vm.gridUpdateDate = new Date().toLocaleString();
                ;

            }
        });
    }

    function fetchAllOrders() {
        console.info("fetching all orders");
        $.ajax({
            contentType: "application/json",
            url: "/orders/getAllOrders",
            type: "GET",
            xhrFields: {withCredentials: true},
            dataType: "json",
            timeout: 5000,
            success: function (response) {
                console.info("data received back is: " + JSON.stringify(response));
                // vm.allOrders =response.length;
                // vm.myData = response;
                vm.chartUpdateDate = new Date().toLocaleString();
            }
        });
    }

   function logout () {
        $.ajax({
            contentType: "application/json",
            url: "/auth/logout",
            type: "POST",
            xhrFields: {withCredentials: true},
            dataType: "json",
            timeout: 5000,
            success: function (response) {
                alert("You have logged out. Click ok to continue...");
                window.location="/admin/adminLogin.html";
            },
            error: function (response) {
                alert("You have logged out with errors. Click ok to continue...");
                window.location="/admin/adminLogin.html";
            }
        });
    }

}]);
