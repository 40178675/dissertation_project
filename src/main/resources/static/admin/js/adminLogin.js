//login page
angular.module('adminLogin', []).controller('adminLoginController', ['$timeout', function ($timeout, $http, $scope) {
    console.info("inside admin login module!");

    var vm = this;
    vm.dataLoading = false;
    vm.login = login;
    vm.username = 'lampros';
    vm.password = 'password1';
    (function initController() {
        // reset login status
        //AuthenticationService.ClearCredentials();
    })();


    function login() {
        console.info("inside admin login button!");

        vm.dataLoading = true;
        // AuthenticationService.Login(vm.username, vm.password, function (response) {
        //     if (response.success) {
        //        // AuthenticationService.SetCredentials(vm.username, vm.password);
        //         $location.path('/');
        //     } else {
        //        // FlashService.Error(response.message);
        //         vm.dataLoading = false;
        //     }
        // });

        // create an object just like the one in model directory
        var user = {
            'username': vm.username,
            'password': vm.password
        };
        //convert object to json
        var json = JSON.stringify(user);
        $.ajax({
            contentType: "application/json",
            url: "/auth/login",
            type: "POST",
            data: json,
            dataType: "json",
            success: function (data) {

                console.info("data received back is: " + JSON.stringify(data));
                alert(data.message);
                $timeout(function () {
                    vm.dataLoading = false;
                }, 1000);

                window.location = data.redirect;
            },
            error: function (jqXHR, textStatus, error) {
                // Handle error here
                alert("ERROR: " + JSON.stringify(jqXHR));
                $timeout(function () {
                    vm.dataLoading = false;
                }, 1000);
            }
        });
    };

}]);