//login page
angular.module('login', []).controller('loginController', ['$timeout', function ($timeout, $http, $scope ) {
    console.info("inside login module!");

   if ( document.cookie.match(/^(.; )?\saccess_token\s*=\s*[^;]+(.*)?$/)){
       console.warn("Cookie found!!");
   }else{
       console.warn("Cookie not found!!");

   }

    var vm = this;
    vm.login = login;
    vm.someFunction = someFunction;
    vm.username = 'lam1';
    vm.password = 'password1';
    (function initController() {
        // reset login status
        vm.dataLoading = false;

        //AuthenticationService.ClearCredentials();
    })();


    function login() {
        console.info("inside login button!");

        vm.dataLoading = true;
        // AuthenticationService.Login(vm.username, vm.password, function (response) {
        //     if (response.success) {
        //        // AuthenticationService.SetCredentials(vm.username, vm.password);
        //         $location.path('/');
        //     } else {
        //        // FlashService.Error(response.message);
        //         vm.dataLoading = false;
        //     }
        // });

        // create an object just like the one in model directory
        var user = {
            'username': vm.username,
            'password': vm.password
        };
        //convert object to json
        var json = JSON.stringify(user);
        $.ajax({
            contentType: "application/json",
            url: "/auth/login",
            type: "POST",
            data: json,
            dataType: "json",
            success: function (data) {

                console.info("data received back is: " + JSON.stringify(data));
                alert(data.message);

                window.location = data.redirect;

                $('#loader').hide();
                $timeout(function () {
                    vm.dataLoading = false;
                }, 1000);
            },
            error: function (jqXHR, textStatus, error) {
                // Handle error here
                alert("ERROR: " + JSON.stringify(jqXHR));

                console.log(vm.dataLoading);
                $timeout(function () {
                    vm.dataLoading = false;
                }, 1000);
            }
        });
    };


   function someFunction() {
        console.info("inside reg button!");

        vm.dataLoading = true;
        // AuthenticationService.Login(vm.username, vm.password, function (response) {
        //     if (response.success) {
        //        // AuthenticationService.SetCredentials(vm.username, vm.password);
        //         $location.path('/');
        //     } else {
        //        // FlashService.Error(response.message);
        //         vm.dataLoading = false;
        //     }
        // });

        // create an object just like the one in model directory
        var user = {
            'username': vm.username,
            'password': vm.password
        };
        //convert object to json
        var json = JSON.stringify(user);
        $.ajax({
            contentType: "application/json",
            url: "/users/getUser",
            type: "GET",
            data: {
                userID: "lampros"
            },
            xhrFields: {withCredentials: true},
            dataType: "json",
            timeout:5000,
            success: function (data,textStatus ) {

                console.info("data received back is: " + JSON.stringify(data));
                alert("data received back is: " + JSON.stringify(data));
                this.dataLoading = false;
                // if (data === "loginPage") {
                //     alert("Login was successful!!");
                //     // TODO
                //     // find a way to redirect to question page if login is successful
                //     // window.location = '/adminPage.html';
                // } else {
                //     alert("Login was failed!!");
                // }
            }
        });


    }


}]);