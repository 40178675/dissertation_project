//main page
angular.module('mainPage', []).controller('mainController', function ($http, $scope) {
    console.info("inside main controller.");
    var self = this;
    self.logout = logout;
    self.sendOrder = sendOrder;
    // $scope.productList=[
    //     {productName:"Product1",productPrice:50.5},
    // ];
    $http.get('/products/getAllProducts').then(function (response) {
        console.info("call made with success!");

        console.info("Size : " + Object.keys(response.data).length);

        //scope way
        //$scope.questionUUID = response.data.questionUUID;
        //$scope.questionBody=response.data.question;
        $scope.productList = response.data;
        console.info(response.data);
        console.info($scope.productList);
    });


    function sendOrder() {
        console.info("sending order...");
        $scope.order=
            {   orderUUID:null,
                orderPostageAddress:"Parsons Green Terrace",
                orderPrice:50.5,
                orderDate:null,
                orderProductList:[
                    {
                    productName:null,
                    productType:null,
                    productUUID:"6701f6ca-6f89-49c8-bb3b-57bd50045156",
                    productPrice:null,
                    productDescription:null,
                    productShortDescription:null,
                    productIngredients:["Sugar","Oil","Flour"]
                    }],
                userID:null
            };
        var json = JSON.stringify($scope.order);
        console.log(json);
        $.ajax({
            contentType: "application/json",
            url: "/orders/addOrder",
            type: "POST",
            xhrFields: {withCredentials: true},
            data: json,
            dataType: "json",
            timeout: 5000,
            success: function (response) {
                alert("Order added successfully!");

              //  window.location = "/login.html";
            },
            error: function (response) {
                alert("Order processing error...");

              //  window.location = "/login.html";
            }
        });
    }


    function logout() {
        console.info("inside logout");
        $.ajax({
            contentType: "application/json",
            url: "/auth/logout",
            type: "POST",
            xhrFields: {withCredentials: true},
            dataType: "json",
            timeout: 5000,
            success: function (response) {
                alert("You have logged out. Click ok to continue...");
                window.location = "/login.html";
            },
            error: function (response) {
                alert("You have logged out with errors. Click ok to continue...");

                window.location = "/login.html";
            }
        });
    }

}).directive("owlCarousel", function () {
    return {
        restrict: 'E',
        transclude: false,
        link: function (scope) {
            scope.initCarousel = function (element) {
                // provide any default options you want
                var defaultOptions = {};
                var customOptions = scope.$eval($(element).attr('data-options'));
                // combine the two options objects
                for (var key in customOptions) {
                    defaultOptions[key] = customOptions[key];
                }
                // init carousel
                $(element).owlCarousel(defaultOptions);
            };
        }
    };
}).directive('owlCarouselItem', [function () {
    return {
        restrict: 'A',
        transclude: false,
        link: function (scope, element) {
            // wait for the last item in the ng-repeat then call init
            if (scope.$last) {
                scope.initCarousel(element.parent());
            }
        }
    };
}]);


//login page
angular.module('login', ['$location', 'AuthenticationService', 'FlashService']).controller('loginController', function ($http, $scope, $location, AuthenticationService, FlashService) {
    console.info("inside login!");

    var vm = this;

    vm.login = login;

    (function initController() {
        // reset login status
        AuthenticationService.ClearCredentials();
    })();

    function login() {
        vm.dataLoading = true;
        // AuthenticationService.Login(vm.username, vm.password, function (response) {
        //     if (response.success) {
        //         AuthenticationService.SetCredentials(vm.username, vm.password);
        //         $location.path('/');
        //     } else {
        //         FlashService.Error(response.message);
        //         vm.dataLoading = false;
        //     }
        // });
    }


});