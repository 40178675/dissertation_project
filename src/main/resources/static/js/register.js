//register page
angular.module('register', []).controller('registerController', ['$location', function ($http, $scope, $location, AuthenticationService, FlashService) {
    console.info("inside register module!");

    var vm = this;
    vm.dataLoading = false;
    vm.register = register;
    vm.firstName = 'lam1';
    vm.lastName = 'val';
    (function initController() {
        // reset login status
        //AuthenticationService.ClearCredentials();
    })();


    function register() {
        console.info("inside register button!");

        vm.dataLoading = true;
        // AuthenticationService.Login(vm.username, vm.password, function (response) {
        //     if (response.success) {
        //        // AuthenticationService.SetCredentials(vm.username, vm.password);
        //         $location.path('/');
        //     } else {
        //        // FlashService.Error(response.message);
        //         vm.dataLoading = false;
        //     }
        // });

        // create an object just like the one in model directory
        var user = {
            'userID': vm.userID,
            'userFullName': vm.firstName +" " +vm.lastName,
            'userPassword': vm.inputPassword,
            'userEmail': vm.userEmail
        };
        //convert object to json
        var json = JSON.stringify(user);
        $.ajax({
            contentType: "application/json",
            url: "/auth/userSignUp",
            type: "POST",
            data: json,
            dataType: "json",
            success: function (data) {

                console.info("data received back is: " + JSON.stringify(data));
                alert("Signed up successfully!!");
                window.location= '/admin/login.html'

                // if (data === "loginPage") {
                //     alert("Login was successful!!");
                //     // TODO
                //     // find a way to redirect to question page if login is successful
                //     // window.location = '/adminPage.html';
                // } else {
                //     alert("Login was failed!!");
                // }
            }
        });
    };


}]);