DROP TABLE IF EXISTS user_table;
DROP TABLE IF EXISTS order_table;
DROP TABLE IF EXISTS product_table;
DROP TABLE IF EXISTS orderproducts_table;
DROP TABLE IF EXISTS userorders_table;
DROP TABLE IF EXISTS ingredients_table;
DROP TABLE IF EXISTS persistent_logins;
DROP TABLE IF EXISTS locked_accounts;

CREATE TABLE user_table (
  userID       VARCHAR(20) PRIMARY KEY,
  userEmail        VARCHAR(50),
  userFullName     VARCHAR(60),
  userMobileNumber LONG,
  userPassword     VARCHAR(150),
  userRole         VARCHAR(30)
);

CREATE TABLE order_table (
  orderUUID           VARCHAR(60) PRIMARY KEY,
  orderPrice          DOUBLE,
  orderDate           DATE,
  orderPostageAddress VARCHAR(50)
);
CREATE TABLE product_table (
  productName             VARCHAR(50),
  productType             VARCHAR(20),
  productUUID             VARCHAR(60) PRIMARY KEY NOT NULL,
  productPrice            DOUBLE,
  productDescription      VARCHAR(300),
  productShortDescription VARCHAR(100)
);

CREATE TABLE orderproducts_table (
  orderUUID   VARCHAR(60),
  productUUID VARCHAR(60),
  PRIMARY KEY (orderUUID, productUUID)
);

CREATE TABLE userorders_table (
  userID    VARCHAR(20),
  orderUUID VARCHAR(60),
  PRIMARY KEY (userID, orderUUID)
);

CREATE TABLE ingredients_table (
  productUUID    VARCHAR(60),
  ingredientName VARCHAR(60),
  PRIMARY KEY (productUUID, ingredientName)
);

CREATE TABLE persistent_logins (
  userID varchar(20) not null,
  lastUsed timestamp not null,
  PRIMARY KEY (userID,lastUsed)
);

CREATE TABLE locked_accounts(
  userID varchar(20) not null,
  islocked BOOLEAN not NULL ,
  lastUpdated TIMESTAMP not null,
  PRIMARY KEY (userID)
);


INSERT INTO user_table (userID, userEmail, userFullName, userMobileNumber, userPassword, userRole)
VALUES ('lampros','myEMail','lamVal',0123456789,'$2a$04$ggqwB3n5fz6Gp3qNB3VZyOLdUbmbRI5f98T9FY/coWllWIMYOj2PC','ADMIN');


INSERT INTO product_table (productName, productType, productUUID, productPrice, productDescription, productShortDescription)
VALUES ('Chocolate Cake','CAKE','6701f6ca-6f89-49c8-bb3b-57bd50045156',31,'LongProductDescriptionShouldBeHere','ShortProductDescriptionShouldBeHere');


INSERT INTO product_table (productName, productType, productUUID, productPrice, productDescription, productShortDescription)
VALUES ('Chocolate Muffin','MUFFIN','8801f6ca-6f89-49c8-aa3b-57bd5004519',13,'LongProductDescriptionShouldBeHere','ShortProductDescriptionShouldBeHere');
